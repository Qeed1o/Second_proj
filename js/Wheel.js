class Wheel{
	constructor(){
		this.variants = ["bonus", "basketball", "tennis", "football"];// Значение секторов
		this.index = 0;
		this.rotatePower = 5; // На сколько градусов поворачивается колесо за одну итерацию.
		this.rotated = 0; // Счётчик поворота колеса. Не менять.
	}

	preload(){
		this.load.image('circle','assets/Game/circle.png');
	}

	create(){
		this.circle = this.add.image(this.world.width/2,this.world.height/2,'circle');
		this.circle.anchor.setTo(0.5);
		this.circle.width = this.world.width;
		this.circle.height = this.world.height;
	}

	play(count){
		if(count >= 1){	
			setTimeout(() => {
				this.circle.angle += this.rotatePower;
				this.rotated += this.rotatePower;

				if(this.rotated >= 360/this.variants.length){
					this.index++;
					this.rotated = 0;
					if(this.index > this.variants.length - 1){
						this.index = 0;
					}
				};

				this.circle.width = this.world.width;
				this.circle.height = this.world.height;
				this.play(count - 1);
			},500/(count+10));
		}else{
			setTimeout(()=>{
				playbtn.style.display = "block"; // Когда колесо закончило крутиться - показываем обратно кнопку.
			},2000) // Чтобы ещё раз не нажали.
			Game.hide();
			if(this.variants[this.index] == "bonus"){BonusBet.show();}
			if(this.variants[this.index] == "basketball"){BasketballBet.show();}
			if(this.variants[this.index] == "football"){FootballBet.show();}
			if(this.variants[this.index] == "tennis"){TennisBet.show();}
			//console.log(this.variants[this.index] + "\nЦифра, которая на колесе:" + (this.index+1));
		}
	}
}