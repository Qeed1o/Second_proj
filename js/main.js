var 
	Menu, Game, FootballBet, BasketballBet, TennisBet, BonusBet, Bet, BetHistory, Shop, Win, Lose, LoseDolaps,
	hideAll, money, wheel, wrapper, WheelScene, playbtn, bonuses, dolaps, increaseDolaps, increaseBonuses, prevScene,

	bets = [];

$(document).ready(()=>{
	localStorage.getItem('dolaps') != null ? dolaps = parseInt(localStorage.getItem('dolaps')) : dolaps = 500;
	localStorage.getItem('bonuses') != null ? bonuses = parseInt(localStorage.getItem('bonuses')) : bonuses = 0;
	playbtn = document.getElementById("PlayButtonGameMenu"); // Кнопка прокрутки колеса
	wrapper = document.getElementById("gameWrapper"); // div-обёртка колеса

	Menu = new Menu(document.getElementById("MainMenu"));
	Game = new Game(document.getElementById("GameMenu"));
	Shop = new Shop(document.getElementById("ShopMenu"));

	BetHistory = new BetHistory(document.getElementById("BetHistory"));
	Bet = new Bet(document.getElementById("BetMenu"));
	FootballBet = new FootballBet(document.getElementById("FootballMenu"),
								  document.getElementsByClassName("betWrapper")[0]);

	BasketballBet = new BasketballBet(document.getElementById("BasketballMenu"),
									  document.getElementsByClassName("betWrapper")[1]);

	TennisBet = new TennisBet(document.getElementById("TennisMenu"),
							  document.getElementsByClassName("betWrapper")[2]);

	BonusBet = new BonusBet(document.getElementById("BonusMenu"));
	Win = new Win(document.getElementById("WinMenu"));
	Lose = new Lose(document.getElementById("LoseMenu"));
	LoseDolaps = new LoseDolaps(document.getElementById("LoseDolapsMenu"));


	document.getElementById("DolapsTextMainMenu").innerHTML = "Валюта: " + dolaps;
	document.getElementById("BonusesTextMainMenu").innerHTML = "Бонусы: " + bonuses;

	playbtn.onclick = function(){
		this.style.display = "none";
		WheelScene.play(Math.floor(300+Math.random()*312));
	}

	hideAll = function(){
		LoseDolaps.hide();
		Win.hide();
		Lose.hide();
		Menu.hide();
		Game.hide();
		BetHistory.hide();
		Bet.hide();
		FootballBet.hide();
		BonusBet.hide();
		TennisBet.hide();
		BasketballBet.hide();
		Shop.hide();
	}

	increaseDolaps = function(val){
		dolaps += parseInt(val);

		if(dolaps <= 0 && bonuses <= 0){
			//LOSE();
			Lose.show();
			return 0;
		}

		if(dolaps <= 0){
			LoseDolaps.show();
		}

		localStorage.setItem('dolaps',dolaps);
	}
	increaseBonuses = function(val){
		bonuses += parseInt(val);

		if(bonuses >= 2500){
			Win.show();
			//WIN();
		}

		if(bonuses <= 0 && dolaps <= 0){
			//LOSE();
			Lose.show();
		}

		localStorage.setItem('bonuses',bonuses);
	}
})