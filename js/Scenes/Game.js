function Game(element){
	this.e = element;

	this.hide = function(){
		this.e.style.display = "none";
		wheel = null;
		wrapper.innerHTML = "";
	}

	this.show = function(){
		this.e.style.display = "block";
		/*console.log(wheel);
		console.log(wheel == null)*/
		if(wheel == null){
			wheel = new Phaser.Game({
				width: wrapper.offsetWidth,
				height: wrapper.offsetWidth,
				renderer: Phaser.CANVAS,
				parent: wrapper,
				transparent: true
			})

			WheelScene = new Wheel;
			wheel.state.add("MAIN",WheelScene);
			wheel.state.start("MAIN");
		}
	}
}