function BetHistory(element){
	this.e = element;

	this.hide = function(){
		this.e.style.display = "none";
	}

	this.show = function(){
		Menu.hide();
		this.e.style.display = "block";
		if(bets.length > 0){
			document.getElementById("storyWrapper").innerHTML = "";
			for(let i = bets.length - 1; i > bets.length - 8; i--){ // Показываем только часть истории ставок
				if(i < 0){
					break;
				}else{
					let div, imgSrc;

					if(bets[i][0] == 'football'){ // Определяем иконку
						imgSrc = "assets/Bets/soccer.png";
					}else if(bets[i][0] == 'basketball'){
						imgSrc = "assets/Bets/basketball.png";
					}else if(bets[i][0] == 'tennis'){
						imgSrc = "assets/Bets/tennis.png";
					}else{
						imgSrc = "assets/Bets/bonus.png";
					}


					if(i == (bets.length - 1)){
						div = "<div style='width:100%; height: 20%;'><img style='width:10%;' src='" + imgSrc +
						"'><div style='height:100%; border-radius: 10px; float: right; width: 90%; padding: 0; background-color:#000;'><h2 style='margin: 0; display:inline-block; float:left;'>"
						+ bets[i][1] + " валюты</h2><h2 style='display:inline-block; margin: 0; float:right;'>" + bets[i][2] + " бонусов</h2></div></div>"
					}else{
						div = "<div style='width:100%; height: 20%;'><img style='width:10%;' src='" + imgSrc +
						"'><div style='height:100%; border-radius: 10px; float: right; width: 90%; padding: 0; background-color:#000;'><h2 style='margin: 0; display:inline-block; float:left;'>"
						+ bets[i][1] + " валюты</h2><h2 style='display:inline-block; margin: 0; float:right;'>" + bets[i][2] + " бонусов</h2></div></div>"
					}
					if(bets[i][1] == 0 && bets[i][2] == 0){
						div = "<div style='width:100%; height: 20%;'><img style='width:10%;' src='" + imgSrc +
						"'><div style='height:100%; border-radius: 10px; float: right; width: 90%; padding: 0; background-color:#000;'><h2 style='margin: 0; margin-left: 25%; display:inline-block; float:left;'> МАТЧ ЕЩЁ НЕ ЗАКОНЧИЛСЯ</h2></div></div>"
					}
					document.getElementById("storyWrapper").innerHTML += div;
			}
		}
	}
}
}