function Menu(element){
	this.e = element;

	this.start = function(){
		hideAll();
		Game.show();
	}

	this.show = function(){
		hideAll();
		this.e.style.display = "block";
		document.getElementById("DolapsTextMainMenu").innerHTML = "Валюта: " + dolaps;
		document.getElementById("BonusesTextMainMenu").innerHTML = "Бонусы: " + bonuses;
	}

	this.hide = function(){
		this.e.style.display = "none";
	};
}