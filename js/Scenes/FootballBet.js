function FootballBet(element, betWindow){
	this.e = element;
	this.betW = betWindow;

	this.hide = function(){
		this.e.style.display = "none";
	}

	this.show = function(){
		prevScene = this;
		this.e.style.display = "block";
	}

	this.win_button = function(){
		let div1, div2, div3, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ПОБЕДА</div>"
		div1 = "<div onclick='FootballBet.bet(\"win\",1.3, 1)' style='display: inline-block; position: relative; width: 20%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>1<br>X1,3</div>"
		div2 = "<div onclick='FootballBet.bet(\"win\",3.4, 0)' style='display: inline-block; position: relative; width: 20%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>X<br>X3,4</div>"
		div3 = "<div onclick='FootballBet.bet(\"win\",2.8, 2)' style='display: inline-block; position: relative; width: 20%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>2<br>X2,8</div>";
		this.betW.innerHTML = header + div1 + div2 + div3;
	}

	this.total_button = function(){
		let div1, div2, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ТОТАЛ</div>"
		div1 = "<div onclick='FootballBet.bet(\"total\",1.9, 1)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>2,5Б<br>X1,9</div>"
		div2 = "<div onclick='FootballBet.bet(\"total\",2.9, 2)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>2,5М<br>X2,9</div>";
		this.betW.innerHTML = header + div1 + div2;
	}

	this.firstSet_button = function(){
		let div1, div2, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ПЕРВЫЙ ТАЙМ</div>"
		div1 = "<div onclick='FootballBet.bet(\"firstSet\",1.7, 1)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>0,5Б<br>X1,7</div>"
		div2 = "<div onclick='FootballBet.bet(\"firstSet\",3.0, 2)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>0,5М<br>X3,0</div>";
		this.betW.innerHTML = header + div1 + div2;		
	}

	this.secondSet_button = function(){
		let div1, div2, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ВТОРОЙ ТАЙМ</div>"
		div1 = "<div onclick='FootballBet.bet(\"secondSet\",2.4, 1)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>1,5Б<br>X2,4</div>"
		div2 = "<div onclick='FootballBet.bet(\"secondSet\",1.7, 2)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>1,5М<br>X1,7</div>";
		this.betW.innerHTML = header + div1 + div2;		
	}

	this.bet = function(str, kf, side){
		this.hide();
		Bet.show('football',str,kf, side);
	}
}