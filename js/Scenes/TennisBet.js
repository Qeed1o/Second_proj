function TennisBet(element, betWindow){
	this.e = element;
	this.betW = betWindow;

	this.hide = function(){
		this.e.style.display = "none";
	}

	this.show = function(){
		prevScene = this;
		this.e.style.display = "block";
	}

	this.win_button = function(){
		let div1, div2, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ПОБЕДА</div>"
		div1 = "<div onclick='TennisBet.bet(\"win\",1.45, 1)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>1<br>X1,45</div>"
		div2 = "<div onclick='TennisBet.bet(\"win\",1.9, 2)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>2<br>X1,9</div>";
		this.betW.innerHTML = header + div1 + div2;
	}

	this.total_button = function(){
		let div1, div2, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ТОТАЛ</div>"
		div1 = "<div onclick='TennisBet.bet(\"total\",2.2, 1)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>2,95Б<br>X2,2</div>"
		div2 = "<div onclick='TennisBet.bet(\"total\",1.5, 2)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>2,95М<br>X1,5</div>";
		this.betW.innerHTML = header + div1 + div2;
	}

	this.firstSet_button = function(){
		let div1, div2, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ПЕРВЫЙ СЕТ</div>"
		div1 = "<div onclick='TennisBet.bet(\"firstSet\",1.26, 1)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>9,5Б<br>X1,26</div>";
		div2 = "<div onclick='TennisBet.bet(\"firstSet\",3.0, 2)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>9,5М<br>X3,0</div>";
		this.betW.innerHTML = header + div1 + div2;
	}

	this.secondSet_button = function(){
		let div1, div2, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ВТОРОЙ СЕТ</div>"
		div1 = "<div onclick='TennisBet.bet(\"secondSet\",1.9, 1)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>11,5Б<br>X1,9</div>";
		div2 = "<div onclick='TennisBet.bet(\"secondSet\",2.5, 2)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>11,5М<br>X2,5</div>";
		this.betW.innerHTML = header + div1 + div2;
	}

	this.bet = function(str, kf, side){
		this.hide();
		Bet.show('tennis',str,kf, side);
	}
}