function BasketballBet(element, betWindow){
	this.e = element;
	this.betW = betWindow;

	this.hide = function(){
		this.e.style.display = "none";
	}

	this.show = function(){
		prevScene = this;
		this.e.style.display = "block";
	}

	this.win_button = function(){
		let div1, div2, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ПОБЕДА</div>"
		div1 = "<div onclick='BasketballBet.bet(\"win\",1.25, 1)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>1<br>X1,25</div>"
		div2 = "<div onclick='BasketballBet.bet(\"win\",2.3, 2)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>2<br>X2,3</div>";
		this.betW.innerHTML = header + div1 + div2;
	}

	this.total_button = function(){
		let div1, div2, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ТОТАЛ</div>"
		div1 = "<div onclick='BasketballBet.bet(\"total\",2.6, 1)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>211,5Б<br>X2,6</div>"
		div2 = "<div onclick='BasketballBet.bet(\"total\",1.6, 2)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>211,5М<br>X1,6</div>";
		this.betW.innerHTML = header + div1 + div2;
	}

	this.firstSet_button = function(){
		let div1, div2, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ПЕРВЫЙ ТАЙМ</div>"
		div1 = "<div onclick='BasketballBet.bet(\"firstSet\",2.2, 1)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>112,5Б<br>X2,2</div>"
		div2 = "<div onclick='BasketballBet.bet(\"firstSet\",1.45, 2)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>112,5М<br>X1,45</div>";
		this.betW.innerHTML = header + div1 + div2;
	}

	this.secondSet_button = function(){
		let div1, div2, header;
		header = "<div style='position: relative; width:40%; margin-left: 30%; background-color: #161616; color: #fff;'>ВТОРОЙ ТАЙМ</div>"
		div1 = "<div onclick='BasketballBet.bet(\"secondSet\",2.8, 1)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; margin-left: 10%; background-color: #72aa81; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>115,5Б<br>X2,8</div>"
		div2 = "<div onclick='BasketballBet.bet(\"secondSet\",1.4, 2)' style='display: inline-block; position: relative; width: 35%; padding-bottom: 5%; padding-top: 5%; background-color: #72aa81; margin-left: 10%; text-align: center; margin-top: 20%; transform: translateY(-50%); color: #000;'>115,5М<br>X1,4</div>";
		this.betW.innerHTML = header + div1 + div2;
	}

	this.bet = function(str, kf, side){
		this.hide();
		Bet.show('basketball',str,kf, side);
	}
}