function Bet(element){
	this.e = element;

	this.hide = function(){
		this.e.style.display = "none";
	}

	this.back = function(){
		this.hide();
		prevScene.show();
	}

	this.show = function(sportname, target, kf, side){
		console.log("PREVSCENE = " + prevScene);
		this.e.style.display = "block";
		this.sportname = sportname;
		this.target = target;
		this.kf = kf;
		this.side = side;
	}

	this.submit = function(){
		let bet =  parseInt(document.getElementById("BetCost").innerHTML),
			kf = this.kf,
			betToPlace = kf*bet;
			sportname = this.sportname;
		increaseDolaps(-bet);
		this.hide();
		Menu.show();

		if(sportname == 'football'){
			bets.push([sportname, 0, 0]);
			let newlen = bets.length - 1;
			setTimeout(() => {
				let rnd = 1 + Math.random() * 100;
				if(rnd >= 50){// Если выиграл ставку.
					increaseDolaps(betToPlace);
					increaseBonuses(betToPlace/4);
					bets[newlen][1] = betToPlace;
					bets[newlen][2] = betToPlace/4;
				}else{// Если проиграл.
					bets.push([sportname, -bet, -bet]);
				}
				console.log("Football bet played!")
			},600000);
		}else if(sportname == 'tennis'){
			bets.push([sportname, 0, 0]); // Не сыгравшая ставка.
			let newlen = bets.length - 1;
			setTimeout(() => { // По истечение времени станет нормальной
				let rnd = 1 + Math.random() * 100;
				if(rnd >= 50){// Если выиграл ставку.
					increaseDolaps(betToPlace);
					increaseBonuses(betToPlace/4);
					bets[newlen][1] = betToPlace;
					bets[newlen][2] = betToPlace/4;
				}else{// Если проиграл.
					bets.push([sportname, -bet, -bet]);
				}
				console.log("Tennis bet played!")
			},300000);
		}else if(sportname == 'basketball'){
			bets.push([sportname, 0, 0]);
			let newlen = bets.length - 1;
			setTimeout(() => {
				let rnd = 1 + Math.random() * 100;
				if(rnd >= 50){// Если выиграл ставку.
					increaseDolaps(betToPlace);
					increaseBonuses(betToPlace/4);
					bets[newlen][1] = betToPlace;
					bets[newlen][2] = betToPlace/4;
				}else{// Если проиграл.
					bets.push([sportname, -bet, -bet]);
				}
				console.log("Basketball bet played!")
			},900000);
		}
	}
}