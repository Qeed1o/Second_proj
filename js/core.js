function updateVisible(css){
	if (css != null) $("body").addClass(css);
}
function toNative(text){
	console.log("To native: " + text);
	document.location = "native:"+text;
}
function toNativeAlert(msg){
	toNative("alert:"+msg);
}
function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max + 1 - min)) + min;
}
function getRequest(params, callback){
	var p = "&";
	$.each(params, function(index, value) {
		p += index + "=" + value + "&";
	});
	$.ajax({
		url: serverUrl+p,
		context: document.body
	}).done(function(d, s) {
		var a = JSON.parse(d);
		if (a.url != undefined){
			// location.href = a.url;
			// return;
		}
		if (a.error == 1){
			alert(a.message);
		}else{
			callback(a.data);
		}
	});
}
function getRequestFile(params, callback){
	$.ajax({
		url: serverUrl + params,
		context: document.body
	}).done(function(d, s) {
		callback(d);
	});
}

function core_init(){
	if (localStorage.udata == undefined){
	}else{
		// getDataForUser(localStorage.udata);
	}
	
}

function setUserID(id){
	localStorage.udata = id;
	getDataForUser(localStorage.udata);
}

function getDataForUser(id){
	getRequest({"action":"getdata", "udata":localStorage.udata});
}