function Menu(element){
	this.e = element;

	this.start = function(){
		this.hide();
		Game.show();
	}

	this.hide = function(){
		this.e.style.display = "none";
	}
}